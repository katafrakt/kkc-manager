# PLEASE NOTE: you don't need this file and task.
# It serves only my personal purpose.

namespace :csv do
	task :import => :environment do
		require 'csv'
		FILENAME = "lista.csv"
		lines = CSV.read(FILENAME)
		header = lines.shift
		season = Season.create!(name: '2013/14', start: Date.new(2013, 8, 20))
		type = TrainingType.create!(name: 'Na lodzie')
		trainings = []
		header.shift
		header.each do |date|
			day, month, year = date.split('.')
			p header
			puts "#{date}: #{day}.#{month}.#{year}"
			d = Date.new(year.to_i, month.to_i, day.to_i)
			trainings << Training.create!(date: d, obligatory: true, training_type: type, season: season)
		end

		lines.each do |member_line|
			name = member_line.shift
			last_name, first_name = name.split(' ')
			sex = first_name.last == 'a' ? 0 : 1
			member = Member.create!(first_name: first_name, last_name: last_name, sex: sex, active: true)

			member_line.each_with_index do |presence, i|
				if presence == 'N'
					next
				elsif presence.nil?
					type = 2
				elsif presence == 'O'
					type = 1
				elsif presence == 'Z'
					type = 4
				elsif presence == 'U'
					type = 3
				else
					raise 'Wrong presence!'
				end
				Presence.create!(member: member, training: trainings[i], presence_type: type)
			end
		end
	end
end
