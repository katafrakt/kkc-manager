require 'test_helper'

class TeamMembershipTest < ActiveSupport::TestCase
  fixtures :teams,:users

  test 'current membership' do
  	TeamMembership.create!(team: teams(:one), member: members(:active), start: Time.now - 1.year, finish: nil)
  	TeamMembership.create!(team: teams(:one), member: members(:active2), start: Time.now - 1.month, finish: Time.now + 2.days)
  	TeamMembership.create!(team: teams(:one), member: members(:inactive), start: Time.now - 1.year, finish: Time.now - 3.months)
  	assert_equal 3, teams(:one).members.count
  	assert_equal 2, teams(:one).current_members.size
  end
end
