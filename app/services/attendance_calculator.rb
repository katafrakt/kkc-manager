class AttendanceCalculator
	def initialize(presences)
		@presences = presences
	end

	def types
		@presences.map{|p| p.event.event_type }.uniq
	end

	def calculate
		attendance = {}
		types.each do |type|
			selection = @presences.select{|p| p.event.event_type == type }
			attendance[type.name] = obligatory_and_total(selection)
		end
		attendance[:total] = obligatory_and_total(@presences)
		attendance
	end

	def obligatory_and_total(presences)
		{obligatory: _calculate(presences.select{|p| p.event.obligatory}), all: _calculate(presences) }
	end

	def _calculate(presences)
		total = presences.size.to_f
		good = presences.select{|p| p.presence_type == 1 || p.presence_type == 4 }.size.to_f
		skipped = presences.select{|p| p.presence_type == 3 }.size.to_f
		return 0 if total-skipped <= 0
		100*good/(total - skipped)
	end
end
