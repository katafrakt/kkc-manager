module TeamsHelper
  def memberships_to_show(team)
    memberships = team.team_memberships.includes(:member)
    team.active ? memberships.select{|m| m.active? } : memberships
  end
end
