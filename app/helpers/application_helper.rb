module ApplicationHelper

  def display_base_errors resource
    return '' if (resource.errors.empty?) or (resource.errors[:base].empty?)
    messages = resource.errors[:base].map { |msg| content_tag(:p, msg) }.join
    html = <<-HTML
    <div class="alert alert-error alert-block">
      <button type="button" class="close" data-dismiss="alert">&#215;</button>
      #{messages}
    </div>
    HTML
    html.html_safe
  end

  def presence_class(presence)
    return "" if presence.nil?
    presence.type_name.downcase
  end

  def format_presence(presence)
    return "" if presence.nil?
    case presence.presence_type
      when 1 then t('presence.abbrev.present')
      when 2 then t('presence.abbrev.absent')
      when 3 then t('presence.abbrev.extenuated')
      when 4 then t('presence.abbrev.competition')
    end
  end

  def administration_links(params)
    render partial: 'shared/administration_links',
      locals: {edit_path: params[:edit], delete_path: params[:delete]}
  end

  def event_title(event)
    return event.title unless event.title.blank?
    "#{event.event_type.name}: #{event.date}"
  end

end
