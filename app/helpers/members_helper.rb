module MembersHelper
	def format_sex(sex)
		if sex == 0
			t('female')
		else
			t('male')
		end
	end

	def format_attendances(attendances)
		total = attendances.delete(:total)
		if attendances.keys.length <= 1
			return {partial: 'members/attendance_simple', locals: {attendance: total}}
		end
	end
end
