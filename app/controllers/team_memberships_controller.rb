class TeamMembershipsController < ApplicationController
  def index
    @team = Team.find(params[:team_id])
    @memberships = @team.team_memberships
  end

  def create
    @team = Team.find(params[:team_id])
    @team.team_memberships.create(safe_params)
    redirect_to @team
  end

  def edit
    @membership = TeamMembership.find(params[:id])
  end

  def update
    @membership = TeamMembership.find(params[:id])
    if @membership.update_attributes(safe_params)
      redirect_to team_path(@membership.team_id)
    else
      render :edit
    end
  end

  def destroy
    @membership = TeamMembership.find(params[:id])
    @membership.destroy
    redirect_to team_path(@membership.team_id)
  end

  private
  def safe_params
    params.require(:team_membership).permit(:start, :finish, :member_id)
  end
end
