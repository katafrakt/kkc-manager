class MembersController < ApplicationController
	def index
		@members = Member.all
	end

	def show
		@member = Member.find(params[:id])
		@presences = @member.presences.joins(:event => :event_type).
			eager_load(:event => :event_type).order('events.date DESC')
		@attendance = AttendanceCalculator.new(@presences).calculate
	end

	def new
		@member = Member.new
	end

	def create
		@member = Member.new(safe_params)
		if @member.save
			redirect_to members_path
		else
			render :new
		end
	end


	def destroy
		member = Member.find(params[:id])
		member.destroy
		redirect_to members_path
	end

	private
	def safe_params
		params.require(:member).permit(:first_name, :last_name, :sex, :active, :active_since)
	end
end
