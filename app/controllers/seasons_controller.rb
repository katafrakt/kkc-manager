class SeasonsController < ApplicationController
	def index
		@seasons = Season.order(start: :desc)
	end

	def show
		@season = Season.find(params[:id])
		@events = @season.events.includes(:event_type).order(date: :desc)
		@event_types = EventType.where(id: @season.events.select(:event_type_id).uniq)
	end

	def attendances
		@season = Season.find(params[:id])
		@events = @season.events.order('date asc').includes(:presences)
		member_ids = @events.map{|t| t.presences.map(&:member_id) }.flatten.uniq
		@members = Member.where(id: member_ids).order('last_name, first_name')
		@member_presences = []
		@members.each do |member|
			presence_ary = []
			@events.each do |event|
				presence_ary << event.presences.select{|p| p.member_id == member.id }.first
			end
			attendance = AttendanceCalculator.new(presence_ary.compact).calculate[:total][:all]
			@member_presences << {member: member, presence: presence_ary, attendance: attendance}
		end
	end

	def new
		@season = Season.new
	end

	def create
		@season = Season.new(sanitized_params)
		if @season.save
			redirect_to seasons_path
		else
			render :new
		end
	end

	def edit
		@season = Season.find(params[:id])
	end

	def update
		@season = Season.find(params[:id])
		if @season.update_attributes(sanitized_params)
			redirect_to seasons_path
		else
			render :edit
		end
	end

	def destroy
		season = Season.find(params[:id])
		season.destroy
		redirect_to seasons_path
	end

	private
	def sanitized_params
		params.require(:season).permit(:name, :start)
	end
end
