class TeamsController < ApplicationController
	def index
		@teams = Team.all
	end

	def show
		@team = Team.find(params[:id])
		@memberships = @team.team_memberships.includes(:member)
	end

	def new
		@team = Team.new
		@team.team_memberships.build
	end

	def create
		@team = Team.new(safe_params)
		if @team.save
			redirect_to teams_path
		else
			render :new
		end
	end

	def edit
		@team = Team.find(params[:id])
	end

	def update
		@team = Team.find(params[:id])
		if @team.update_attributes(safe_params)
			redirect_to teams_path
		else
			render :edit
		end
	end

	private
	def safe_params
		params.require(:team).permit(:name, :founded, :active, :members)
	end
end
