class EventsController < ApplicationController
	def index
	end

	def show
		@event = Event.find(params[:id])
		@presences = @event.presences.eager_load(:member)
	end

	def new
		@season = Season.find(params[:season_id])
		@event = @season.events.build
		@event.date = Date.today
	end

	def create
		@season = Season.find(params[:season_id])
		@event = @season.events.build(safe_params)
		if @event.save
			redirect_to season_event_path(@season, @event)
		else
			render :new
		end
	end

	def edit
		@season = Season.find(params[:season_id])
		@event = @season.events.find(params[:id])
	end

	def update
		@event = Event.find(params[:id])
		if @event.update_attributes(safe_params)
			redirect_to season_event_path(@event.season_id, @event)
		else
			render :edit
		end
	end


	private
	def safe_params
		params.require(:event).permit(:title, :description, :date, :season_id, :event_type_id)
	end

end
