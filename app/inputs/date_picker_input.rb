# It can't be DatepickerInput because it clashes with some naming conventions
# in gem's CSS, rendering input invisible
class DatePickerInput < SimpleForm::Inputs::Base
  def input
    text_field_options = input_html_options.dup

    text_field_options[:class] << 'datepicker_input'
    text_field_options['data-date-format'] = "yyyy-mm-dd"

    return_string = @builder.text_field(attribute_name, text_field_options)
    return return_string.html_safe
  end
end
