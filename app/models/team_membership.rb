class TeamMembership < ActiveRecord::Base
	belongs_to :team
	belongs_to :member

	def active?
		now = Time.now
		(start.nil? || start < now) && (finish.nil? || finish > now)
	end

	# if no start date is given it is assumed that it is equal
	# to team founding date
	def start
		attribute(:start) || self.team.founded
	end
end
