class Member < ActiveRecord::Base
	has_many :presences
	has_many :events, :through => :presences
	has_many :team_memberships
	has_many :teams, :through => :team_memberships
	
	validates :first_name, presence: true
	validates :last_name, presence: true
	validates :sex, presence: true, inclusion: [0,1]

	def full_name
		"#{first_name} #{last_name}"
	end

	def current_team
		team_participations.where(['start < ? AND (finish > ? OR finish IS NULL)', Time.now, Time.now]).first.try(:team)
	end
end
