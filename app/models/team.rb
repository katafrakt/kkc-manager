class Team < ActiveRecord::Base
	has_many :team_memberships
	has_many :members, :through => :team_memberships
	accepts_nested_attributes_for :team_memberships

	def current_members
		members.where('start < ? AND (finish > ? OR finish IS NULL)', Time.now, Time.now)
	end
end
