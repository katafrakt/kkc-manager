class Event < ActiveRecord::Base
	belongs_to :season
	belongs_to :event_type
	has_many :presences
	has_many :members, :through => :presences

	validates :event_type, presence: true
	validates :season, presence: true
end
