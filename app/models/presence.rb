class Presence < ActiveRecord::Base
	belongs_to :member
	belongs_to :event

	PRESENCE_TYPES = {
		1 => 'Present',
		2 => 'Absent',
		3 => 'Extenuated',
		4 => 'Competition'
	}

	def type_name
		PRESENCE_TYPES[presence_type]
	end
end
