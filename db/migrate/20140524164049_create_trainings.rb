class CreateTrainings < ActiveRecord::Migration
  def change
    create_table :trainings do |t|
      t.date :date
      t.integer :training_type_id
      t.boolean :obligatory
      t.integer :season_id
      t.hstore :data
      t.json :cache

      t.timestamps
    end
  end
end
