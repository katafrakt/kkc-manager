class CreatePresences < ActiveRecord::Migration
  def change
    create_table :presences do |t|
      t.integer :member_id
      t.integer :training_id
      t.integer :presence_type

      t.timestamps
    end
  end
end
