class TransformTrainingsIntoEvents < ActiveRecord::Migration
  def change
    rename_table :trainings, :events
    rename_column :events, :training_type_id, :event_type_id
    rename_table :training_types, :event_types
    rename_column :presences, :training_id, :event_id
  end
end
