class CreateMembers < ActiveRecord::Migration
  def change
    reversible do |mig|
      mig.up { execute "create extension if not exists hstore"}
      mig.down {}
    end

    create_table :members do |t|
      t.string :first_name
      t.string :last_name
      t.date :birth_date
      t.integer :sex
      t.boolean :active
      t.date :active_since
      t.hstore :data

      t.timestamps
    end
  end
end
