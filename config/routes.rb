Rails.application.routes.draw do
  resources :teams do
    resources :team_memberships
  end

  resources :presences

  resources :training_types

  resources :seasons do
  	resources :events
  	get :attendances, :on => :member
  end

  resources :members

  root to: 'home#index'

  devise_for :users
end
